import { Component, ViewContainerRef } from '@angular/core';
// Services
import { TostrMessageService } from './@share/messages/api/tostr-message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})

export class AppComponent {
  title = 'app';


  constructor(
    private tostrMessageService: TostrMessageService,
    private _vcr: ViewContainerRef) {

  }


private showSuccess() {
  this.tostrMessageService.add('Success, You are on right track.', 'success');
}

}
