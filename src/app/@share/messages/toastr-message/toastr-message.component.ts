import { Component, OnInit } from '@angular/core';
// Services
import { TostrMessageService } from '../api/tostr-message.service';
// Models
import { Message } from '../view-model/message';

@Component({
  selector: 'app-toastr-message',
  templateUrl: './toastr-message.component.html',
  styleUrls: ['./toastr-message.component.less']
})
export class ToastrMessageComponent implements OnInit {

  messages: Message;

  constructor(private tostrMessageService: TostrMessageService) { }

  ngOnInit() {
    this.tostrMessageService.add('My first message.', 'info');
    this.messages = this.tostrMessageService.getMessages();
  }

}
