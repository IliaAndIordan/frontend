import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToastrMessageComponent } from './toastr-message.component';

describe('ToastrMessageComponent', () => {
  let component: ToastrMessageComponent;
  let fixture: ComponentFixture<ToastrMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToastrMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastrMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
