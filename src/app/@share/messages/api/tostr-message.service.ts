import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Message } from '../view-model/message';

@Injectable()
export class TostrMessageService {

  private messages: Message[] = [];

  constructor(private toastrService: ToastrService) { }

  public add(message: string, style: string): void {
    const msg = new Message(message, style);
     this.messages.push(msg);
    if ( msg.style === 'info') {
      this.toastrService.info(msg.content);
    } else if ( msg.style === 'error') {
      this.toastrService.error(msg.content);
    } else {
      this.toastrService.info(msg.content);
    }


    // this.messages.push(message);
  }

  public getMessages(): any {
    return this.messages.filter(x => x.dismissed = false);
    }
  }
