import { TestBed, inject } from '@angular/core/testing';

import { TostrMessageService } from './tostr-message.service';

describe('TostrMessageServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TostrMessageService]
    });
  });

  it('should be created', inject([TostrMessageService], (service: TostrMessageService) => {
    expect(service).toBeTruthy();
  }));
});
