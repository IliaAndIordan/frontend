export class Message {

    content: string;
    style: string;
    // tslint:disable-next-line:no-inferrable-types
    dismissed: boolean = false;

    constructor(content, style?) {
      this.content = content;
      this.style = style || 'info';
    }

}
