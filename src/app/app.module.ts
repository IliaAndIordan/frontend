import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { TostrMessageService } from './@share/messages/api/tostr-message.service';


import { AppComponent } from './app.component';
import { ToastrMessageComponent } from './@share/messages/toastr-message/toastr-message.component';


@NgModule({
  declarations: [
    AppComponent,
    ToastrMessageComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AlertModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [TostrMessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
